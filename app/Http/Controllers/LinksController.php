<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\Sites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LinksController extends Controller
{
    public function save(Request $request)
    {
        $links = $request->input('links');
        foreach ($links as $link) {
            if (!Link::where('url', $link)->exists()) {
                $savedLinks[] = Link::create([
                    'url' => $link
                ]);
            }
        }
        return response()->json(
            [
                'success' => true,
                'links' => $links
            ]
        );
    }

    public function check(Request $request)
    {
        $output = [];
        $links = $request->input('links');
        $accurance = $request->input('accurance');
        foreach ($links as $link) {
            $existingLink = Link::where('url', $link)->first();
            if ($existingLink) {
                $path = base_path("public\photoScript.py --name $link --accurance $accurance");
                $rawPath = shell_exec("python $path");
                $notEtalon = str_replace('\n', '', basename($rawPath));
                $etalon = str_replace('.png', 'e.png', basename($rawPath));
                $diff = str_replace('.png', '_difference.png', basename($rawPath));
                $data = [
                    'prev_url' => 'images/' .  $etalon,
                    'current_url' => 'images/' . $notEtalon,
                    'result_url' => 'images/' .  $diff,
                    'site_url' => $link,
                    'isAproved' => 0
                ];
                if (!Sites::where('current_url', 'images/' . $notEtalon)->exists()) {
                    Sites::create($data);
                }

                $data['prev_url'] = Storage::url($data['prev_url']);
                $data['current_url'] = Storage::url($data['current_url']);
                $data['result_url'] = Storage::url($data['result_url']);

                $output[] = $data;
            } else {
                return response()->json(['success' => false, 'error' => 'No such link in database']);
            }
        }
        return response()->json([
            'result' => $output
        ]);
    }

    public function deleteSite($id)
    {
        $site = Sites::find($id);
        if (!$site) {
            return response()->json(['message' => 'Site not found'], 404);
        }
        $site->delete();
        return response()->json(['message' => 'Site images deleted successfully']);
    }

    public function isAproved($id)
    {
        $site = Sites::findOrFail($id);

        if (!$site) {
            return response()->json(['message' => 'Site not found'], 404);
        }
        $site->update(['isAproved' => 1]);
        return response()->json([
            'success' => true,
            'message' => 'Запись успешно обновлена.'
        ]);
    }

    public function getById($id)
    {
        $data = Sites::findOrFail($id);

        if (!$data) {
            return response()->json(['message' => 'Site not found'], 404);
        }
        $data['prev_url'] = Storage::url($data['prev_url']);
        $data['current_url'] = Storage::url($data['current_url']);
        $data['result_url'] = Storage::url($data['result_url']);
        return response()->json([
            'success' => true,
            'result' => $data
        ]);
    }

    public function sites()
    {
        $sites = Sites::all();
        return response()->json(['sites' => $sites]);
    }

    public function links()
    {
        $links = Link::all();
        return response()->json(['links' => $links]);
    }
}
