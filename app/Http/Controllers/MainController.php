<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MainController extends Controller
{
    public function logIn(Request $request)
    {
        $email = $request->input('email');
        if (Auth::attempt($request->only('email', 'password'), $request->filled('remember'))) {
            $user = Auth::user();
            $user_id = $user->id;
            $data = [
                'success' => true,
                'email' => $email,
                'user_id' => $user_id
            ];
            $token = substr(sha1(mt_rand()), 17, 6);
            $data['token'] = $token;
            Token::create([
                'id_user' => $user_id,
                'token' => substr(sha1(mt_rand()), 17, 6)
            ]);
            return response()->json($data);
        }

        return response()->json([
            'success' => false,
            'error' => 'Wrong email or password'
        ]);
    }

    public function registerUser(Request $request)
    {
        if (User::where('email', $request->email)->exists()) {
            return  response()->json([
                'success' => false,
                'error' => 'Bad request'
            ]);
        }
        event(new Registered($user = $this->create($request->all())));

        $user_id = $user->id;
        $data = [
            'success' => true,
            'email' => $request->email,
            'user_id' => $user_id
        ];

        $token = substr(sha1(mt_rand()), 17, 6);
        Token::create([
            'id_user' => $user_id,
            'token' => substr(sha1(mt_rand()), 17, 6)
        ]);
        $data['token'] = $token;
        return response()->json($data);
    }

    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'role' => 'user',
            'password' => Hash::make($data['password']),
        ]);
    }
}
