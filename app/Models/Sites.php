<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sites extends Model
{
    use HasFactory;

    protected $table = 'sites';

    protected $fillable = [
        'prev_url',
        'current_url',
        'result_url',
        'site_url',
        'isAproved'
    ];
}
