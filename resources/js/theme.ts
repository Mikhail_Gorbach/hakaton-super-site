import { createTheme } from "@mui/material";

const theme = createTheme({
    typography: {
        h6: {
            fontWeight: 700,
            fontSize: 15,
            color: "#fff",
        },
        h2: {
            fontWeight: 700,
            fontSize: 28,
            color: "#000",
        },
        subtitle1: {
            fontSize: 15,
            fontWeight: 500,
            color: "red",
        },
    },
    components: {
        MuiTextField: {
            styleOverrides: {
                root: {
                    padding: 0,
                },
            },
        },
    },
});

export default theme;
