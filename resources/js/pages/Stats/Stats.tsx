import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import {
    approveSite,
    deleteSite,
    getImage,
    getSites,
    selectImages,
    selectIsLoading,
    selectSites,
} from "../../redux/images/imageSlice";
import styles from "./stats.module.scss";
import classNames from "classnames/bind";
import Loader from "../../components/Loader/Loader";
import { Button, Typography } from "@mui/material";

const cx = classNames.bind(styles);

function Stats() {
    const dispatch = useAppDispatch();
    const loading = useAppSelector(selectIsLoading);
    const images = useAppSelector(selectImages);
    const sites = useAppSelector(selectSites);
    useEffect(() => {
        dispatch(getSites());
    }, [dispatch]);

    const getPath = (image, attr: any) => {
        const str = image[attr];
        let path = str.replace("\\", "");
        path = path.substring(0, path.length - 1);
        path = path.replace("storage/public", "");
        path = path.replace("storage", "");
        console.log(path);

        const url = "http://localhost:5173/public/" + path;

        return url;
    };

    if (loading) {
        return <Loader />;
    }
    return (
        <div className={cx("stats__container")}>
            {sites.map((image) => {
                return (
                    <>
                        <div className={cx("stats__head")}>
                            <Button
                                size="medium"
                                variant="contained"
                                color="success"
                                disabled={image.isAproved}
                                onClick={async () => {
                                    await dispatch(approveSite(image.id));
                                    dispatch(getSites());
                                }}
                            >
                                Апрув
                            </Button>
                            <Typography variant="body1" color="#000">
                                {image.site_url}
                            </Typography>
                            <Button
                                size="medium"
                                color="error"
                                variant="contained"
                                disabled={image.isAproved}
                                onClick={async () => {
                                    await dispatch(deleteSite(image.id));
                                    dispatch(getSites());
                                }}
                            >
                                Ошибка
                            </Button>
                        </div>
                        <Typography variant="body1" color="#000">
                            Новая картинка
                        </Typography>
                        <div className={cx("stats__image")}>
                            <div
                                style={{
                                    width: "80%",
                                }}
                            >
                                <img
                                    alt="txt"
                                    src={getPath(image, "current_url")}
                                ></img>
                            </div>
                        </div>
                        <Typography variant="body1" color="#000">
                            Старая картинка
                        </Typography>
                        <div className={cx("stats__image")}>
                            <div
                                style={{
                                    width: "80%",
                                }}
                            >
                                <img
                                    alt="txt"
                                    src={getPath(image, "prev_url")}
                                ></img>
                            </div>
                        </div>
                        <Typography variant="body1" color="#000">
                            Разница картинок
                        </Typography>
                        <div className={cx("stats__image")}>
                            <div
                                style={{
                                    width: "80%",
                                }}
                            >
                                <img
                                    alt="txt"
                                    src={getPath(image, "result_url")}
                                ></img>
                            </div>
                        </div>
                    </>
                );
            })}
        </div>
    );
}

export default Stats;
