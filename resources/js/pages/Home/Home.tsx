import React, { useEffect } from "react";
import styles from "./home.module.scss";
import classNames from "classnames/bind";
import DataTable from "../../components/DataTable/DataTable";
import MultipleSelect from "../../components/MultipleSelect";
import { SelectChangeEvent, TextField, Typography } from "@mui/material";
import { Field, Form, Formik } from "formik";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import {
    getLinks,
    selectCurrentLinks,
    selectIsLoading,
    selectSiteLinks,
    sendLinks,
} from "../../redux/links/linkSlice";
import { getImage } from "../../redux/images/imageSlice";
import { LoadingButton } from "@mui/lab";
import AlertWrapper from "../../components/AlertWrapper";

const cx = classNames.bind(styles);

function Home(): React.ReactElement<React.FC> {
    const dispatch = useAppDispatch();
    const siteLinks = useAppSelector(selectSiteLinks);
    const loading = useAppSelector(selectIsLoading);

    useEffect(() => {
        dispatch(getLinks());
    }, [dispatch]);

    const handleSaveLinks = async (links: string[]): Promise<void> => {
        await dispatch(sendLinks(links));
        dispatch(getLinks());
    };

    return (
        <div className={cx("home__container")}>
            <Typography variant="h2">Проверить соответвие сайта</Typography>

            <Formik
                enableReinitialize
                initialValues={{
                    links: [],
                    newLinks: "",
                }}
                onSubmit={(values: string[]) => {
                    if (values.links.length > 0) {
                        handleSaveLinks({
                            links: values.links,
                        });
                        dispatch(
                            getImage({
                                links: values.links,
                            })
                        );
                    }
                    if (values.newLinks !== "") {
                        const array = [];
                        array.push(values.newLinks);

                        handleSaveLinks({
                            links: array,
                        });
                        dispatch(
                            getImage({
                                links: array,
                            })
                        );
                    }
                }}
            >
                {({ setFieldValue, submitForm }) => (
                    <Form>
                        <div className={cx("home__form")}>
                            <Field
                                name="newLinks"
                                as={TextField}
                                label="Вводите URL"
                            />
                            <Field
                                name="links"
                                as={MultipleSelect}
                                label="Выберите URL"
                                options={siteLinks}
                                onChange={(
                                    event: SelectChangeEvent<string[]>
                                ) => {
                                    const {
                                        target: { value },
                                    } = event;
                                    setFieldValue(
                                        "links",
                                        // On autofill we get a stringified value.
                                        typeof value === "string"
                                            ? value.split(",")
                                            : value
                                    );
                                }}
                            />
                            <LoadingButton
                                loading={loading}
                                size="large"
                                variant="contained"
                                color="success"
                                onClick={submitForm}
                            >
                                Проверить сайты
                            </LoadingButton>
                        </div>
                    </Form>
                )}
            </Formik>
            <div className={cx("home__table")}>
                <DataTable options={siteLinks} />
            </div>
            <AlertWrapper error="Ваш токен для Telegram: asdtgj542sf" />
        </div>
    );
}

export default Home;
