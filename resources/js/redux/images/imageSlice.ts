import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import axios from "axios";

interface ImageState {
    prev_url: string;
    current_url: string;
    result_url: string;
    site_url: string;
    isAproved: number;
}

interface imageState {
    loading: boolean;
    error: null | string;
    images: ImageState[];
    success: boolean;
    sites: ImageState[];
}

const initialState: imageState = {
    loading: false,
    error: null,
    images: [],
    success: false,
    sites: [],
};

export const getImage = createAsyncThunk(
    "images/get",
    async (obj: string[], { rejectWithValue }) => {
        try {
            const response = await axios.post("/api/url", obj);

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const getSites = createAsyncThunk(
    "sites/get",
    async (obj: string[], { rejectWithValue }) => {
        try {
            const response = await axios.get("/api/sites");

            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const deleteSite = createAsyncThunk(
    "sites/delete",
    async (id: number, { rejectWithValue }) => {
        try {
            return await axios.delete(`/api/sites/${id}`);
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const approveSite = createAsyncThunk(
    "sites/approve",
    async (id: number, { rejectWithValue }) => {
        try {
            return await axios.put(`/api/sites/${id}`);
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const imageSlice = createSlice({
    name: "images",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getImage.pending, (state) => {
                return {
                    ...state,
                    loading: true,
                };
            })
            .addCase(getImage.fulfilled, (state, action) => {
                return {
                    ...state,
                    loading: false,
                    images: action.payload.result,
                };
            })
            .addCase(getSites.pending, (state) => {
                return {
                    ...state,
                    loading: true,
                };
            })
            .addCase(getSites.fulfilled, (state, action) => {
                return {
                    ...state,
                    loading: false,
                    sites: action.payload.sites,
                };
            });
    },
});

export const selectImages = (state: RootState): ImageState[] =>
    state.images.images;
export const selectIsLoading = (state: RootState): boolean =>
    state.images.loading;
export const selectSites = (state: RootState): ImageState[] =>
    state.images.sites;

export default imageSlice.reducer;
