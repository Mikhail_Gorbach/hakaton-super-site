import * as React from "react";
import styles from "./datatable.module.scss";
import classNames from "classnames/bind";
import { Typography } from "@mui/material";

const cx = classNames.bind(styles);

interface IDataTable {
    options: string[];
}

export default function DataTable(props: IDataTable) {
    return (
        <div className={cx("datatable__container")}>
            {props.options.map((option) => (
                <a
                    key={option.id}
                    href={option.url}
                    className={cx("datatable__option")}
                >
                    <Typography variant="h6" color="#000">
                        {option.url}
                    </Typography>
                </a>
            ))}
        </div>
    );
}
