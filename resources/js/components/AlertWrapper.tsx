import React, { ReactElement, useEffect, useState } from "react";
import { Alert, Snackbar, Typography } from "@mui/material";

interface AlertProps {
    error: string;
}

function AlertWrapper(props: AlertProps): ReactElement<React.FC> {
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (props.error) {
            setOpen(true);
        }
    }, [props.error]);

    return (
        <Snackbar
            open={open}
            anchorOrigin={{
                horizontal: "center",
                vertical: "top",
            }}
            autoHideDuration={5000}
            onClose={() => {
                setOpen(false);
            }}
        >
            <Alert severity="error">
                <Typography variant="body2">{props.error}</Typography>
            </Alert>
        </Snackbar>
    );
}

export default AlertWrapper;
