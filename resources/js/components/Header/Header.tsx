import React from "react";
import styles from "./header.module.scss";
import classNames from "classnames/bind";
import { Link } from "react-router-dom";
import { routes } from "../../routeConfig";
import { Button, Typography } from "@mui/material";
import { useAppDispatch } from "../../redux/hooks";
import { logOut } from "../../redux/auth/authSlice";

const cx = classNames.bind(styles);

function Header(): React.ReactElement<React.FC> {
    const dispatch = useAppDispatch();
    const handleLogout = () => {
        dispatch(logOut());
    };
    return (
        <div className={cx("header__container")}>
            <Link to={routes.home}>
                <Typography variant="h6">Главная</Typography>
            </Link>
            <Link to={routes.stats}>
                <Typography variant="h6">Сводка</Typography>
            </Link>
            <Button variant="text" size="large" onClick={handleLogout}>
                <Typography variant="h6">Выйти</Typography>
            </Button>
        </div>
    );
}

export default Header;
