import React from "react";
import styles from "./loader.module.scss";
import classNames from "classnames/bind";
import { LoadingButton } from "@mui/lab";

const cx = classNames.bind(styles);

function Loader() {
    return (
        <div className={cx("loader__area")}>
            <LoadingButton size="large" loading={true} />
        </div>
    );
}

export default Loader;
