export const routes = {
    base: "/",
    login: "/login",
    home: "/home",
    stats: "/stats",
};
