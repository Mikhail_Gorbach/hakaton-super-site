import argparse
import os
from pathlib import Path
from platform import system
from urllib.parse import urlparse

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from sys  import argv
from PIL import Image, ImageChops, ImageOps 
import os
import json

def resize_image(image, new_width, new_height):
    # Открываем изображение 
    # Создаем новое изображение с белым фоном
    new_image = Image.new("RGB", (new_width, new_height), "white")
    # Вычисляем координаты для размещения исходного изображения
    x = round((new_width - image.size[0]) / 2)
    y = 0
    # Размещаем исходное изображение на новом изображении
    new_image.paste(image, (x, y))
    return new_image

def count_nonwhite_pixels(image):
    # Получаем размеры изображения
    width, height = image.size

    # Счетчик не белых пикселей
    count = 0

    # Проходим по всем пикселям изображения
    for y in range(height):
        for x in range(width):
            # Получаем цвет пикселя
            pixel = image.getpixel((x, y))

            # Проверяем, является ли пиксель не белым
            if not all(c in range(240,255) for c in pixel[:3]):
                count += 1
    print(count)
    return count

options = Options()
options.add_argument("--headless")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
options.add_argument("--disable-blink-features=AutomationControlled")

exec_path = os.path.join(os.getcwd(), 'driver', 'chromedriver.exe') if system() == "Windows" else \
    os.path.join(os.getcwd(), 'driver', 'chromedriver')

driver = webdriver.Chrome(options=options, service=Service(log_path=os.devnull, executable_path=exec_path))

driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
    'source': '''
        delete window.cdc_adoQpoasnfa76pfcZLmcfl_Array;
        delete window.cdc_adoQpoasnfa76pfcZLmcfl_Promise;
        delete window.cdc_adoQpoasnfa76pfcZLmcfl_Symbol;
  '''
})


def screen_sait(url) -> None:
    path = os.path.join("images", str(urlparse(url).hostname).replace(Path(str(urlparse(url).hostname)).suffix, ".png"))
    driver.get(url)
    s = lambda x: driver.execute_script('return document.body.parentNode.scroll' + x)
    driver.set_window_size(s('Width'), s('Height'))
    driver.find_element(By.TAG_NAME, 'body').screenshot(path)
    driver.close()
    driver.quit()
    return path

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str, required=True, help='Your name')
parser.add_argument('--accurance', type=int, required=True, help='Your name')
args = parser.parse_args()
path = screen_sait(args.name)

path = path[:len(path)-4] 
isExists = os.path.exists(path + 'e.png')
image=Image.open(path + '.png')
if isExists == True:
    image_e = Image.open(path + 'e.png')
    width1 ,height1 = image.size 
    width2 ,height2 = image_e.size
    neww = max(width1,width2)
    newh = max (height1,height2)
    image = resize_image(image, neww, newh)
    image_e = resize_image(image_e,neww,newh)
    result=ImageChops.difference(image, image_e)
    result = ImageOps.invert(result)
    count = count_nonwhite_pixels(result)
    result.save(path + '_difference.png')
    if ((count * (newh * neww)) / 100) >= 95:
      image.save(path + 'e.png') 
    final = path + '.png'
else:
    image.save(path + 'e.png')
    final = path + '.png'

print(final)


# url
# фото эталон
# последнее фото 
# фото сравнение